[[_TOC_]]

# Abstract

# 1 Introduction

**Our mission at DEMU is to help bring music into Web3.**

It is difficult to succinctly describe all of the benefits of Web3 or how this new technology is predicted to transform our world. This isn't merely a new Facebook. Or Twitter. Or Amazon. This is an upgrade to the ***internet***. And it has been coined "Web3".

Web3 will allow people to transact value with one another across the internet, in a *trustless* fashion, minus the need for banks and other expensive intermediaries - computer code will programmatically guarantee we do what we agree to do.

Web3, built on the blockchain, promises a future of data sovereignty. This means anyone will be able to connect and disconnect to any Web3-compatible website (or app, or software) and take their data with them where they go. Imagine a Web3 version of Facebook, where one can take with them every post they've ever created if they ever decided to leave the platform. This is only a part of what this *upgraded internet*, Web3, will empower people to do.

Internet users of the future will use a digital wallet to interact with these Web3 applications. This wallet will hold digital money, digital IDs, credentials, passwords, and other digital "stuff" as a web browser plug-in and on mobile devices. When the internet user connects their wallet to a website (or app, or other software), they'll be able to transact or interact with the website directly, without sharing their personal data, and without banks. Consumers are tired of being spied on and manipulated. The people *want this freedom*. Businesses will need to be prepared for this inevitable future. Our mission at DEMU is to help elevate music so that all participants enjoy the benefits of Web3.

## 1.1 Current problems

**How can Web3 solve some of the problems in the business of music?**

It is well chronicled how unfair the music industry can be for artists. Record labels haven't always done right by the artists, then streaming services come along and shrink revenue even more. What's worse, an invisible hand is eating away at an artist's profit - $450M to a centralized storage solution is what one streaming service pays to host and distribute their library of music. This is only part of the reason why artists are being paid $.003 per play on average. Many independent artists get paid much less. Finally, there are music publishing companies that monitor the uses of the artists' music and collect royalties on their behalf, a process which takes 6-12 months to get paid and incurs additional fees.

The promise of Web3, and our DEMU platform, is that we can disintermediate this entire legacy structure. Artists can become *their own gatekeepers*. We contend that record labels are still essential, dependent on the goals of the artist. We believe that the increased transparency created by DEMU will lead to fairer record deals. If artists decide to go independent, they could form a record label of their own and leverage DEMU just the same.

## 1.2 The DEMU project

DEMU is a Web3 Music protocol built on the Cardano blockchain. Our goal is to automate the direct payment of music royalties to the rights holders worldwide. We want to become the financial backbone of the music business.

The protocol can support an ecosystem of additional Web3 tools, products & services to empower music professionals to connect directly with their fans. These tools will include Music streaming, NFT marketplaces, social tokens, and other crypto products made available to artists via their websites, apps, and eventually, the metaverse. And with the challenge of artist royalties solved, developers can focus on building their music blockchain startups atop DEMU.

The first product built on the DEMU protocol is project name: Jukebox. It is a music player connected to the blockchain. It will work like a digital jukebox. Pay a token. Play a song. Royalties get paid directly to the rights holders of the music. Fans save while supporting their favorite artists by paying directly.

We hope that with DEMU, the benefits of our Web3 Music Protocol become clear. It is about returning control over the art back to the rights holders of that art. And to enrich the experience of being a fan and supporting the artists you love.

# 2 Token Model

Many different stakeholders with different goals will use the DEMU protocol. For these diverse stakeholders to effectively work together toward common network goals, there needs to be a unified incentive structure that aligns the interests of the individual with the interests of the protocol. Two different tokens with distinct properties dictate this incentive structure.

The DEMU platform token ($DEMU) powers the DEMU Protocol.

The PLAY music access token ($PLAY) will have its value pegged to a yet-to-be-determined stablecoin and is used to purchase or stream music.

![token-model.png](images/token-model.png)

## 2.1 DEMU Token ($DEMU)

DEMU platform tokens (ticker $DEMU) have three prongs of functionality within the protocol unlocked by staking:

- Security
- Feature access
- Governance

DEMU can be staked as collateral for providing value-added services to the network. In exchange, stakers earn ongoing issuance and governance weight. This staking will be done by:

- node operators
- client developers
- independent creators

Node operators must stake DEMU tokens to operate any infrastructure node. A more significant stake correlates to a higher probability of being chosen by user clients. Node operators receive direct upside from seeding in the form of $DEMU and a share of protocol fees for actively seeding the protocol.

A community goal, via governance, is to ensure that DEMU tokens are always being funneled to the most value-added actors by using on-chain metrics as a measurement, rather than simply to those staking the most tokens but not actively participating in the ecosystem.

### 2.1.1 DEMU Token Distribution

DEMU will be distributed with fixed genesis allocation and by ongoing issuance modifiable by governance

The choice to launch with ongoing issuance is grounded in the desire to continually align the network's growth with new actors and their relative contributions rather than concentrating governance power in the hands of early actors.

Should the community see fit, a portion of ongoing token issuance will be allocated to the most active users of the protocol, dictated based on platform metrics and varying contributions in the form of discoverability, streams, and platform engagement.

The initial allocation of tokens upon genesis will be described in a separate but linked document.

## 2.2 PLAY Payment Token ($PLAY)

Users can acquire $PLAY tokens by swapping a yet-to-be-determined stablecoin. $PLAY tokens value will be pegged to the value of this stablecoin once it has been decided.

Once acquired, $PLAY tokens can be deposited and used to purchase or stream the music creators make available on the network. Creators will receive their payment and royalties in the form of $PLAY token, which can be traded manually or automatically for the original stablecoin.

## 2.3 Creator / Social Tokens

The project team also foresees DEMU providing a direct mechanism for infrastructure providers and creators to better engage their community by distributing creator / social tokens. Creators could have the ability to distribute a unique token directly through DEMU, giving users who hold a specified amount of those tokens the ability to access exclusive features.

## 2.4 Why create a new token?

For value transfer in the DEMU ecosystem, third-party stablecoins allow micropayments to occur in real-time without oversight from a trusted third party to facilitate the distribution, accounting, or collection of royalties and network fees. Stablecoins are pegged 1:1 to the US Dollar, providing a trusted unit of account with the inherent benefits of smart-contract composability.

DEMU tokens exist to align governance and financial incentives that increase protocol usage and create long-term protocol value and be a method of value transfer to make purchases on the DEMU market.

Participation in governance as a node operator, provider, or user, allows stakers to earn a claim on future issuance, incentivizing value-added actors to increase protocol usage to drive demand back to DEMU tokens.

Extendable assets like creator tokens allow DEMU to play an active role in the wider Cardano and Defi ecosystem without recreating the tokens, tooling, and primitives.

# 3 Architecture

The Demu protocol will be run by a distributed network of complementary interacting nodes of different types that will expose the protocol APIs to the public internet and be accessed via official or third-party developed domain-specific client interfaces. There will be two different node types, Discovery (4) and Content (2). They will be run by Stakepool operators willing to stake $DEMU token for permission to run these nodes in return for a share in the networks' revenues. It may not be necessary to be a Stakepool operator to run a node in the future.

![architecture-overview.png](images/architecture-overview.png)

# 3.1 Content Node

Content nodes maintain the availability of artifacts and metadata in DEMU on DEMUsp, the DEMU-native extension to [IPFS](https://ipfs.io/). These nodes can be run by node operators alongside an active network stake, allowing them to earn part of the ongoing DEMU token issuance and aggregated fee pools.

By default, a creator's client elects a set of these nodes to maintain the availability of content automatically on the creator's behalf—the vast majority of creators do not need to have any knowledge of this process.

When relying on the 3rd-party network of DEMU content nodes, this set evolves automatically after electing an initial set of nodes. New nodes replace old ones that are taken offline or become unavailable.

## 3.1.1 DEMUsp: A decentralized storage protocol

Files distributed through the DEMU protocol must be highly available, independently verifiable, and decentralized. These principles are crucial to ensuring democratic participation and accessibility for all users of the DEMU protocol.

## 3.1.2 Upload flow

To distribute a song on DEMU, creators must agree to the DEMU open license (we will publish this license in a separate brief), making the content available on the broader DEMU network. The creators' client will then:

1. Slice the file into fixed-length segments
2. Encrypt them locally (if the content is permissioned) with segment-specific keys
3. Upload these encrypted segments, the encryption keys, and required metadata to their content node(s)

The content node(s) then publish the content and metadata to DEMUSP. An IPLD link is produced for the metadata and added to the DEMU content ledger via a new transaction (See Section 3.2 for more on this process). The discovery nodes then index the new content to make it more broadly discoverable and available (see Section 3.3).

## 3.1.3 Content permissioning

In addition to maintaining content availability, content nodes also take responsibility for permissioning access to content.

The content permissioning system in DEMU aims to be:

1. Transparent for all parties involved
2. Cost- and time-efficient for all transactions
3. Flexible, accounting for multiple access models and any monetization scheme the creator sees fit
4. Granular, with users paying each other directly and immediately for services rendered when possible

As described in the upload flow in Section 3.1.2, if the content is permissioned, the creator's client generates encryption keys for the content shared with / managed by the content node(s) elected by the creator. Because of DEMUSP, anyone can now fetch the encrypted content being kept available by the selected content nodes. Proxy re-encryption allows a content node to issue a key to a given user upon request selectively.

### 3.1.4 Proxy re-encryption

When beginning to request a song, a consumer's client will request a proxy re-encryption key specific to the song being consumed from one of DEMU's elected content nodes by sending payment or other proof of purchase.

The content node derives a proxy re-encryption key using the consumer's public wallet key and the private key used to encrypt the requested song and returns it to the consumer to service this request. Because the re-encryption key is specific to the creator, consumer, and segment, it can be transmitted insecurely or published without revealing the song contents to the broader network.

The cryptosystem used to encrypt tracks will allow the issuance of fan-specific proxy re-encryption keys derived from the track encryption key and the fan's public key. The producer's content node(s) will handle key requests and issue new keys when the specified conditions are met, issuing a new key by mixing the track encryption key with the consumer's wallet's public key.

After fetching encrypted content and a re-encryption key, the consumer client would locally decrypt the content using their wallet private key as follows:

proxied = reencrypt(encrypted_content,reencryption_key)

plaintext = decrypt(proxied,wallet_privkey)

This key decrypts a given content by locally re-encrypting it using the key mentioned above and subsequently decrypting it with the consumers' private key.

There is no 3rd-party proxy, but proxy re-encryption applied in this way allows everyone to share the same encrypted content while consumers can only decrypt the content on a case-by-case basis.

Potential cryptosystems are still being evaluated at this time.

# 3.2 Content Ledger

The DEMU content ledger, referred to throughout the paper, is the amalgamation of smart contracts on Cardano and other future L1 or L2 blockchain networks that host pieces of the DEMU ecosystem. Different parts of the DEMU protocol will continue to run on different blockchain-based platforms or utilize off-chain scalability solutions, where scalability trilemma tradeoffs [need reference 15] can be made on a module and subprotocol-specific basis.

Initially, the content ledger for DEMU should include:

- Consistent track content and metadata format specification to ensure accessibility (similar to the OMI metadata spec [16, § 3.7.1-3.7.2])
- A decentralized process for creators to control: – Track content – Revenue splits – Content ownership structure
- A registry of all nodes reachable in DEMU
- The social graph of all users interacting with DEMU
- Implementations of the token and governance systems described in this paper

After generating upload artifacts from their content nodes, as described in Section 3.1, a creator can add their content to the content ledger via a new transaction.

The creator can then modify track content/metadata by sharing the modified content to DEMUSP and updating the metadata IPLD link in the content ledger. Once the content is listed in the content ledger, it is indexed by the discovery nodes as detailed in Section 3.3, making it easily queryable and discoverable by clients accessing DEMU.

## 3.2.1 Node registry

The DEMU content ledger maintains a single source of truth for

1. all the valid versions of node software usable within DEMU, controlled by governance (Section 4),
2. all the discovery and content nodes reachable within DEMU, and
3. how to find them (via IP address or fully qualified domain name).

When a client connects to DEMU for the first time, it can use this on-chain registry to bootstrap its local state (e.g., looking up which account maps to the active wallet, what is the current user's social graph/feed via a chosen discovery node)

## 3.2.2 Social features and fan feed

The content ledger also serves as a central source of truth for consumer/creator interactions happening within DEMU. Users can take the following actions within DEMU:

- retrieve a track
- Like a track, adding it to the consumers' library
- Follow other consumers and creators, and receive notifications when they create new songs
- Create a public (shared with followers) or private tracklist
- Repost tracks to followers

More action types can be added in the future via community governance. Actions taken by users get organized into user-specific feeds that reflect the time-sorted actions of the other users they follow—this is enabled by the indexing functionality described in the next section. All social activities within DEMU are represented in the content ledger, meaning users can use any client to connect to DEMU and see the same social graph. Consumers can also view what other consumers have been using, as developers can when building third-party clients. This opens up many possibilities around content recommendation systems and alternative client experiences constructed by the DEMU developer community members.

## 3.2.3 Tokens and governance

The DEMU content ledger is also the home of the $DEMU and $PLAY token economies (Section 2) and DEMU governance system (Section 4), described in their respective sections.

# 3.3 Discovery Node

For a user to discover content on the network, DEMU needs a mechanism for indexing metadata efficiently queryable by users. Based on the philosophies of the DEMU project, this index must be:

- Decentralized
- Efficient and straightforward for user clients to consume (promoting accessibility)
- Provably correct and transparent, eliminating profit incentives to manipulate the results returned to users
- Extensible so that the DEMU community can explore different ranking and searching methodologies.

Due to usability and efficiency issues, these requirements rule out the most decentralized options, e.g., users replicating the DEMU ledger locally and querying their local dataset. This section outlines a protocol for a class of discovery nodes to form operated by the DEMU community, serving this function to meet the above requirements.

Discovery node operators earn revenue by registering a node with an active network stake, letting them earn part of the ongoing DEMU token issuance and aggregated fee pools. User clients select discovery nodes to query via the content ledger's node registry (Section 3.2.1). Discovery nodes are read-only. Clients can use them to fetch a user's feed, a tracklist, track metadata, search the corpus of DEMU entities, and execute other queries about the network. Anyone can register a discovery node if they meet the requirements outlined in this section.

## 3.3.1 Discovery API interfaces

DEMU will produce a first-party discovery API interface, but other community members are encouraged to author interfaces that extend or modify the core API. The protocol allows users to select any discovery API interface registered in the DEMU content ledger.

An API interface must index new blocks from the DEMU ledger atomically (i.e., all-or-nothing), and all API methods must be deterministic. Because of these requirements, all discovery nodes running a given API interface will produce identical results for the same query for a given block hash. This consistency guarantee is essential for the penalty mechanism described in Section 4.2.

## 3.4 Future work

We foresee the community creating an incentive economy around discovery node interfaces, which would allow the creator/maintainer of an interface to earn a portion of rewards earned by node operators using this interface. Node rewards could also be tied to the number of requests fielded to incentivize nodes to operate with higher-quality infrastructure and in locations near large population centers.

# 4 Governance

Integral to achieving this mission is a decentralized governance protocol, whereby creators, node operators, and consumers are individually and collectively enfranchised in decision-making about protocol changes and upgrades.

In the spirit of creating a community-owned and operated streaming protocol, these actors should be empowered to shape, mend and modify underlying parameters of the DEMU protocol, including but not limited to:

- Feature Integrations
- Reward Rates
- Token Distribution
- Fee Pool Allocation
- Staking Rewards

Everything in DEMU is governable, and all DEMU tokens staked in the protocol automatically receive governance weight. The precise amount of voting power per token staked is yet to be determined.

DEMU politicians differ because node operators are unique from producers and consumers, both of which are aligned in the growth of the protocol. Governance will look to present technical and non-technical proposals, giving all users the ability to properly voice their beliefs without running a node or having a deep technical understanding of the DEMU tech stack.

For node operators, DEMU governance acts as a critical tool to empower decentralized content storage, providing a direct mechanism for rewards to be earned and amended according to the costs, value, and consensus of other providers on the network. By creating a framework for users to adjust the direction of the protocol in line with their shared beliefs, DEMU will curate governance to the most value-added actors, possibly tying in incentives to those who are most active. In doing so, core DEMU politicians will play an active role in future token issuance, node operator incentives, and creative mechanisms for producers to better engage the consumer community.

## 4.1 Short-circuiting

A short-circuit process allows both 1) proposals to be passed without a broader vote if urgency requires, e.g., during active exploitation of a vulnerability in the protocol, and 2) proposals to be vetoed if they are not consistent with the philosophies outlined in this paper.

A community multisig will control this short-circuit capability with an initial set of signers, and additional signers can be voted into place via the open community governance process. The community, at any time, can vote to remove the ability to short-circuit governance if they choose, and the controllers of the short-circuit multisig will be committed not to veto said proposal when the time comes to relinquish control.

The project team will add this functionality to the governance process with the intention for it to be removed—it is up to the community to decide when it makes sense to take off the training wheels or whether it makes sense to have this functionality at all.

## 4.2 Enforcing node response accuracy

Every response a community-operated node in DEMU returns is signed with the private key that was used to stake the original tokens or a designated delegate, the block hash of the block they have incorporated up to that point (for discovery nodes), or a timestamp (for content nodes), inputs provided to query, and the node software version from the public version registry they are using to generate results.

Blocks are indexed atomically in discovery nodes, and API methods are deterministic, meaning that every discovery node should produce identical results for the same query, block hash, and API interface.

The content node cannot provide as strong guarantees by timestamp as the discovery node can by block numbers and deterministic indexing. However, sequences of requests with responses (ordered by timestamp) signed by the operator key can provide a similar function for provably demonstrating the behavior.

If either node type produces invalid or inaccurate results, the signed result document returned by the node is self-contained proof that the given node produced the given set of results. Using these proofs as evidence, a claimant could open a governance proposal to slash a given node operator for this alleged misbehavior. The governance process can decide whether there is indeed inaccuracy, whether said inaccuracy was caused by negligence or maliciousness vs. a system error, and slash the node operator's stake accordingly. Slashed tokens are burned rather than redistributed to stakers to avoid incentivizing slashes.

# 5 Roadmap

## 5.1 Genesis

## 5.2 Private Alpha

## 5.3 Testnet

## 5.4 Mainnet Launch

## 7 References

# TODOS:

- enter roadmap once complete
- enter references
